import 'package:flutter/material.dart';

import 'data/locator.dart';
import 'pages/commons/my_app_theme.dart';
import 'pages/screens/splash/splash_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setup();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pusher Demo',
      theme: MyAppTheme.ligthTheme(context: context),
      home: const SplashScreen(),
    );
  }
}
