// ignore_for_file: constant_identifier_names

import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceHelper {
  static const String firsttime = "FIRSTTIME";
  static const String pusher_token = "PUSHERTOKEN";
  final SharedPreferences prefs;

  SharedPreferenceHelper({required this.prefs});

  Future<void> setFirstTime({required bool userfirsttime}) async {
    await prefs.setBool(firsttime, userfirsttime);
  }

  bool? getFirstTime() {
    final userfirsttime = prefs.getBool(firsttime);
    return userfirsttime;
  }

  Future<void> setToken({required String token}) async {
    await prefs.setString(pusher_token, token);
  }

  String? getToken() {
    final token = prefs.getString(pusher_token);
    return token;
  }

  Future<void> removeToken() async {
    await prefs.remove(pusher_token);
  }
}
