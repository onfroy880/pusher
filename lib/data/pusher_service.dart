// ignore_for_file: avoid_print, unused_catch_stack, unused_local_variable

import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:pusher_channels_flutter/pusher_channels_flutter.dart';

class PusherService {
  PusherService({required this.actualUserID});
  final String actualUserID;
  final _pusherChanneInstance = PusherChannelsFlutter.getInstance();
  late final channel = "private-channel_$actualUserID";
  late final authEndpoint =
      "http://192.168.100.4:8000/api/minfo/authen_user_pusher";
  // late final PrivateChannel privateChannel;
  void onConnectPressed() async {
    print(onConnectPressed);
    try {
      await _pusherChanneInstance.init(
        apiKey: "b4894487aa2244af397f",
        cluster: "eu",
        onConnectionStateChange: onConnectionStateChange,
        onError: onError,
        onSubscriptionSucceeded: onSubscriptionSucceeded,
        onEvent: onEvent,
        onSubscriptionError: onSubscriptionError,
        onDecryptionFailure: onDecryptionFailure,
        onMemberAdded: onMemberAdded,
        onMemberRemoved: onMemberRemoved,
        onSubscriptionCount: onSubscriptionCount,
        authEndpoint: authEndpoint,
        onAuthorizer: onAuthorizer,
      );
      await _pusherChanneInstance.subscribe(channelName: channel);
      await _pusherChanneInstance.subscribe(
          channelName: "private-backend_channel");
      // privateChannel = PrivateChannel(
      //     "backend_channel",
      //     Pusher(
      //         APP_KEY,
      //         Options(
      //           authEndpoint: authEndpoint,
      //           cluster: CLUSTER,
      //         )));
      // await privateChannel.subscribe();
      await _pusherChanneInstance.connect();
    } catch (e, s) {
      print(e);
    }
  }

  dynamic onAuthorizer(String channelName, String  socketId, dynamic options) async {
    try {
      var result = await http.post(
        Uri.parse("http://192.168.100.4:8000/api/minfo/authen_user_pusher"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Token 3a4a32500a0b82f1327957e6240cb203ace3e3de',
        },
        body: json.encode({"socket_id": socketId}),
      );
      if (result.statusCode.toString().startsWith('2')) {
        var data = json.decode(result.body);
        return data;
      } else {
        return null;
      }
    } on Exception catch (e) {
      print(e);
      return null;
    }
  }

  void onConnectionStateChange(
      dynamic currentState, dynamic previousState) async {
    switch (currentState.toString()) {
      case "MOBILE_TRANS_UPDATE":
        return;
      case "RECHARGE_TRANS_UPDATE":
        return;
      case "ACCOUNT_UPDATE":
        return;
      default:
        //Todo implement this
        return;
    }
  }

  void onError(String message, int? code, dynamic e) {
    print(e);
  }

  Future<void> onEvent(PusherEvent event) async {
    final time = DateTime.now();
    print(event);
    if (event.channelName == channel) {
      final Map<String, dynamic> data = json.decode(event.data
          .toString()
          .replaceAll("\n", "")
          .replaceAll("\r", "")
          .replaceAll(r"\n", "")
          .replaceAll(r"\r", ""));
      // switch (event.eventName) {
      //   case "MOBILE_TRANS_UPDATE":
      //     try {
      //       final res = MobileTransfer.fromJson(data["data"]);
      //       final controller = Get.find<AppController>();
      //       final index = controller.transList.value?.indexWhere((e) =>
      //               e.mobileTransfer != null &&
      //               e.mobileTransfer!.id == res.id) ??
      //           -1;
      //       if (index != -1) {
      //         controller.transList.value![index] =
      //             controller.transList.value![index]..mobileTransfer = res;
      //         controller.transList.refresh();
      //         controller.refreshAccountList();
      //         print("MobileTransfer Update completed");
      //       } else {
      //         print("rr");
      //       }
      //     } catch (e, s) {
      //       print("MobileTransfer Update failed");
      //     }
      //     return;
      //   case "RECHARGE_TRANS_UPDATE":
      //     try {
      //       final res = RechargeTransfer.fromJson(data["data"]);
      //       final controller = Get.find<AppController>();
      //       final index = controller.transList.value?.indexWhere((e) =>
      //               e.rechargeTransfer != null &&
      //               e.rechargeTransfer!.id == res.id) ??
      //           -1;
      //       if (index != -1) {
      //         controller.transList.value![index] =
      //             controller.transList.value![index]..rechargeTransfer = res;
      //         controller.transList.refresh();
      //         controller.refreshAccountList();
      //         print("RechargeTransfer Update completed");
      //       } else {
      //         print("RechargeTransfer not found");
      //       }
      //     } catch (e, s) {
      //       print("RechargeTransfer Update failed");
      //     }
      //     return;
      //   case "ACCOUNT_UPDATE":
      //     try {
      //       final res = UserBusiness.fromJson(data["data"]);
      //       if (res.status == "deleted") logout();
      //       Get.find<AppController>().userBusiness.value = res;
      //       print("UserBusiness Update completed");
      //     } catch (e, s) {
      //       print("UserBusiness Update failed");
      //     }
      //     return;
      //   default:
      //     //Todo implement this
      //     return;
      // }
    } else {
      //todo impleement this
    }
  }

  void onSubscriptionSucceeded(String channelName, dynamic data) {
    print("channelName: $channelName data: $data");
  }

  void onSubscriptionError(String message, dynamic e) {
    print("message: $message Exception: $e");
  }

  void onDecryptionFailure(String event, String reason) {
    print("event: $event reason: $reason");
  }

  void onMemberAdded(String channelName, PusherMember member) {
    print("channelName: $channelName user: $member");
  }

  void onMemberRemoved(String channelName, PusherMember member) {
    print("channelName: $channelName user: $member");
  }

  void onSubscriptionCount(String channelName, int subscriptionCount) {
    print("channelName: $channelName subscriptionCount: $subscriptionCount");
  }


  void onTriggerEventPressed(
      {required String event,
      required String channel,
      required Map<String, dynamic> data}) async {
    _pusherChanneInstance.trigger(
        PusherEvent(channelName: channel, eventName: event, data: data));
  }

  Future<void> disconnect() => _pusherChanneInstance.disconnect();

  Future<void> askTransUpdate(List<int> list) async {
    _pusherChanneInstance.trigger(PusherEvent(
      channelName: "private-backend_channel",
      eventName: "client-get_topup_update_event",
      data: jsonEncode(list),
    ));
    print("Send to pusher sucessfully");
  }
}
