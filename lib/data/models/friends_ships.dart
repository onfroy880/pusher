// ignore_for_file: non_constant_identifier_names

class FriendShips {
    String? id;
    String? created;
    String? friend;
    String? date_at;

    FriendShips({
        this.id,
        this.created,
        this.friend,
        this.date_at,
    });

    FriendShips copyWith({
        String? id,
        String? created,
        String? friend,
        String? date_at,
    }) => 
        FriendShips(
            id: id ?? this.id,
            created: created ?? this.created,
            friend: friend ?? this.friend,
            date_at: date_at ?? this.date_at,
        );

    factory FriendShips.fromJson(Map<String, dynamic> json) => FriendShips(
        id: json["id"],
        created: json["created"],
        friend: json["friend"],
        date_at: json["date_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "created": created,
        "friend": friend,
        "date_at": date_at,
    };
}
