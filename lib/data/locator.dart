import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'shared_preferences/shared_preferences_helper.dart';

final getIt = GetIt.instance;
Future<void> setup() async {
  // shared preferences
  final prefs = await SharedPreferences.getInstance();
  getIt.registerSingleton<SharedPreferenceHelper>(
    SharedPreferenceHelper(prefs: prefs),
  );
}
