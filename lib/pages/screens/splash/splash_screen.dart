// ignore_for_file: avoid_print

import 'package:flutter/material.dart';

import '../../../data/locator.dart';
import '../../../data/pusher_service.dart';
import '../../../data/shared_preferences/shared_preferences_helper.dart';
// import '../message/message_screen.dart'; 

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final locator = getIt.get<SharedPreferenceHelper>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(child: Container()),
          Container(
            padding: const EdgeInsets.all(16),
            child:  const Center(
              child:  Text(
                  "Pusher Testing",
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.w600,
                  ),
                ),
            ),
          ),
          Image.asset('assets/1.jpg'),
          IconButton.filled(
            onPressed: (){
                  PusherService(actualUserID: "544").onConnectPressed();
                },
            icon: const Icon(Icons.arrow_forward_outlined),
          ),
          Expanded(child: Container()),
        ],
      ),
    );
  }
}
