// ignore_for_file: deprecated_member_use, file_names

import 'package:flutter/material.dart';

class MyAppTheme {
  static ThemeData ligthTheme({required BuildContext context}) {
    return ThemeData(
      brightness: Brightness.light,
      scaffoldBackgroundColor: Colors.white,
      appBarTheme: const AppBarTheme(
        backgroundColor: Colors.white,
        foregroundColor: Colors.blue,
        elevation: 0,
        centerTitle: true,
        actionsIconTheme: IconThemeData(
          color: Colors.blueGrey,
        ),
      ),
      drawerTheme: const DrawerThemeData(
        backgroundColor: Colors.white,
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.blue),
          foregroundColor: MaterialStateProperty.all(Colors.white),
        ),
      ),
      iconButtonTheme: IconButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.blue),
          iconColor: MaterialStateProperty.all(Colors.white),
          textStyle: MaterialStateProperty.all(
            const TextStyle(color: Colors.white),
          ),
          elevation: const MaterialStatePropertyAll(3),
        ),
      ),
      textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.blue),
          iconColor: MaterialStateProperty.all(Colors.white),
          textStyle: MaterialStateProperty.all(
            const TextStyle(color: Colors.white),
          ),
        ),
      ),
      floatingActionButtonTheme: const FloatingActionButtonThemeData(
        backgroundColor: Colors.blue,
        foregroundColor: Colors.white,
      ),
      bottomNavigationBarTheme: const BottomNavigationBarThemeData(
        backgroundColor: Colors.white,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        elevation: 0,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.grey,
        type: BottomNavigationBarType.fixed,
      ),
      fontFamily: "Roboto",
      // backgroundColor: MyColors.secondary.withOpacity(0.3),
      inputDecorationTheme: InputDecorationTheme(
        contentPadding: const EdgeInsets.all(10),
        errorStyle: const TextStyle(fontSize: 8),
        focusedErrorBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          borderSide: BorderSide(color: Colors.red),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(Radius.circular(15)),
          borderSide: BorderSide(color: Colors.blue.withOpacity(0.3)),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(Radius.circular(15)),
          borderSide: BorderSide(color: Colors.blue.withOpacity(0.3)),
        ),
        errorBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          borderSide: BorderSide(color: Colors.red),
        ),
      ),
    );
  }

  // static ThemeData darkTheme({required BuildContext context}) {
  //   return ThemeData(
  //     brightness: Brightness.dark,
  //     scaffoldBackgroundColor: MyColors.darkback,
  //     appBarTheme: AppBarTheme(
  //       backgroundColor: MyColors.darkback,
  //       elevation: 3,
  //       centerTitle: true,
  //       actionsIconTheme: IconThemeData(
  //         color: MyColors.darkback,
  //       ),
  //     ),
  //     elevatedButtonTheme: ElevatedButtonThemeData(
  //       style: ButtonStyle(
  //         backgroundColor: MaterialStateProperty.all(MyColors.primary),
  //         foregroundColor: MaterialStateProperty.all(Colors.white),
  //       ),
  //     ),
  //     iconButtonTheme: IconButtonThemeData(
  //       style: ButtonStyle(
  //         backgroundColor: MaterialStateProperty.all(MyColors.primary),
  //         iconColor: MaterialStateProperty.all(Colors.white),
  //         textStyle: MaterialStateProperty.all(
  //           const TextStyle(color: Colors.white),
  //         ),
  //       ),
  //     ),
  //     textButtonTheme: TextButtonThemeData(
  //       style: ButtonStyle(
  //         backgroundColor: MaterialStateProperty.all(MyColors.primary),
  //         iconColor: MaterialStateProperty.all(Colors.white),
  //         textStyle: MaterialStateProperty.all(
  //           const TextStyle(color: Colors.white),
  //         ),
  //       ),
  //     ),
  //     floatingActionButtonTheme: FloatingActionButtonThemeData(
  //       backgroundColor: MyColors.primary,
  //       foregroundColor: Colors.white,
  //     ),
  //     bottomNavigationBarTheme: BottomNavigationBarThemeData(
  //       backgroundColor: MyColors.darkback,
  //       showSelectedLabels: false,
  //       showUnselectedLabels: false,
  //       elevation: 3,
  //       selectedItemColor: MyColors.secondary.withOpacity(0.3),
  //       unselectedItemColor: Colors.grey.withOpacity(0.3),
  //       type: BottomNavigationBarType.fixed,
  //     ),
  //     fontFamily: "Roboto",
  //     backgroundColor: MyColors.darkback,
  //     searchBarTheme: SearchBarThemeData(
  //       backgroundColor: const MaterialStatePropertyAll(Colors.white),
  //       hintStyle:
  //           MaterialStatePropertyAll(TextStyle(color: MyColors.darkback)),
  //       textStyle:
  //           MaterialStatePropertyAll(TextStyle(color: MyColors.darkback)),
  //     ),
  //     inputDecorationTheme: InputDecorationTheme(
  //       contentPadding: const EdgeInsets.all(10),
  //       labelStyle: const TextStyle(color: Colors.white),
  //       prefixIconColor: Colors.white,
  //       suffixIconColor: Colors.white,
  //       errorStyle: const TextStyle(fontSize: 8),
  //       focusedErrorBorder: const OutlineInputBorder(
  //         borderRadius: BorderRadius.all(Radius.circular(15)),
  //         borderSide: BorderSide(color: Colors.red),
  //       ),
  //       focusedBorder: OutlineInputBorder(
  //         borderRadius: const BorderRadius.all(Radius.circular(15)),
  //         borderSide: BorderSide(color: MyColors.secondary.withOpacity(0.3)),
  //       ),
  //       enabledBorder: OutlineInputBorder(
  //         borderRadius: const BorderRadius.all(Radius.circular(15)),
  //         borderSide: BorderSide(color: MyColors.secondary.withOpacity(0.3)),
  //       ),
  //       errorBorder: const OutlineInputBorder(
  //         borderRadius: BorderRadius.all(Radius.circular(15)),
  //         borderSide: BorderSide(color: Colors.red),
  //       ),
  //     ),
  //   );
  // }
}
